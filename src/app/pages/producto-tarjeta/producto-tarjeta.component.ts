import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ProductoModel } from 'src/app/models/producto.model';

@Component({
  selector: 'app-producto-tarjeta',
  templateUrl: './producto-tarjeta.component.html',
  styleUrls: ['./producto-tarjeta.component.scss']
})
export class ProductoTarjetaComponent implements OnInit {

  productos: ProductoModel[] = [];

  @Input() producto: any = {};
  @Input() index: number = 0;
  @Output() productoSeleccionado: EventEmitter<object>;


  constructor() {

    this.productoSeleccionado = new EventEmitter();

   }

  ngOnInit(): void {
  }

  borrarProducto() {

    this.productoSeleccionado.emit( {producto: this.producto, index: this.index} );

  }  

}
