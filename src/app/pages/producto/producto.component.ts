import { formatCurrency } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ProductoModel } from 'src/app/models/producto.model';
import { ProductosService } from 'src/app/services/productos.service';

@Component({
  selector: 'app-producto',
  templateUrl: './producto.component.html',
  styleUrls: ['./producto.component.scss']
})
export class ProductoComponent implements OnInit {

  forma:FormGroup = this.fb.group({});
  productoObtenido: any = {};
  producto: ProductoModel = new ProductoModel();
  productos: ProductoModel[] = [];

  selected:string = '';
  producto1: ProductoModel[] = [];

  constructor(
    private productoService: ProductosService, 
    private fb: FormBuilder,
    private router:Router
    ) { }

  ngOnInit(): void {
    this.postProduct();
    this.productoService.getProductos()
      .subscribe( (resp:any) => {
        this.producto1 = resp;
      });

  }

  get productoNoValido(){
    return this.forma.get('name')?.invalid && this.forma.get('name')?.touched
  };

  get precioNoValido(){
    return this.forma.get('price')?.invalid && this.forma.get('price')?.touched
  };

  cargar(valor:any){
    this.productoService.getProducto(valor)
    .subscribe((resp => {
      this.productoObtenido = resp;
      if (this.selected) {
          this.router.navigateByUrl(`producto/${this.productoObtenido.id}`)
      } else {
        this.router.navigateByUrl(`producto/nuevo`)
      }
    }))
  }

  guardar(){
    this.productoService.crearProducto(this.forma.value).subscribe( resp => { 
    });
       
  }

  postProduct(){
    this.forma = this.fb.group({
      id:[this.productoObtenido.id],
      name:['', [Validators.required]],
      price:['', [Validators.required]],
      img:['']
    })

  }


}
