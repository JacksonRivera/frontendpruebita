import { Component, OnInit } from '@angular/core';
import { ProductoModel } from 'src/app/models/producto.model';
import { ProductosService } from 'src/app/services/productos.service';

@Component({
  selector: 'app-productos',
  templateUrl: './productos.component.html',
  styleUrls: ['./productos.component.scss']
})
export class ProductosComponent implements OnInit {

  productos: ProductoModel[] = [];

  constructor( private productoService: ProductosService) { }

  ngOnInit(): void {

    this.productoService.getProductos()
      .subscribe( (resp:any) => {
        this.productos = resp;
      });

  }

  borrarProducto( producto:any ) {

    this.productos.splice(producto.index, 1);
    this.productoService.borrarProducto(producto.producto.id).subscribe();

  }  

}
