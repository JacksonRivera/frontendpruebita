import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ProductoModel } from '../models/producto.model';
import { map } from 'rxjs/operators'


@Injectable({
  providedIn: 'root'
})
export class ProductosService {


  private url = 'http://localhost:3000'

  constructor( private http: HttpClient) {}

  /* producto.img.replace(/^.*[\\\/]/, '') */
  crearProducto(producto: any){
    this.http.post(`${this.url}/productos`, producto);
    console.log(producto.img);  
    return this.http.post(`${this.url}/productos/upload/${32}`, producto.img);
  }

  actualizarProducto(producto: ProductoModel){
    return this.http.put(`${this.url}/productos/${ producto.id }`, producto);
  }

  getProductos(){
    return this.http.get(`${this.url}/productos`);
  }

  getProducto(id:string){
    return this.http.get(`${this.url}/productos/${id}`);
  }
 
 
  borrarProducto( id:number){
    return this.http.delete(`${this.url}/productos/${ id }`);
  }

}
